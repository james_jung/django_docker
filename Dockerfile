FROM python:3.8
WORKDIR /usr/src/app

# Required pkgs installation
COPY requirements.txt ./
RUN pip install --upgrade pip
RUN pip install -r requirements.txt

# Project files copy
COPY . .

# Port setting
EXPOSE 8000

# gunicorn execution
CMD ["gunicorn", "--bind", "0.0.0.0:8000", "docker_pjt.wsgi:application"]